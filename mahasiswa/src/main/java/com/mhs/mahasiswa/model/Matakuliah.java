package com.mhs.mahasiswa.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "matakuliahs")
public class Matakuliah {
    @Id
    @Column(name = "mk_id")
    private Long mk_id;

    @Column(name = "rk_id")
    private Long rk_id;

    @Column(name = "ds_id")
    private Long ds_id;

    @Column(name = "mhs_id")
    private Long mhs_id;

    @Column(name = "mk_name", length = 100)
    private String mk_name;

    @Column(name = "duration")
    private int duration;

    public Long getMk_id() {
        return mk_id;
    }

    public void setMk_id(Long mk_id) {
        this.mk_id = mk_id;
    }

    public Long getRk_id() {
        return rk_id;
    }

    public void setRk_id(Long rk_id) {
        this.rk_id = rk_id;
    }

    public Long getDs_id() {
        return ds_id;
    }

    public void setDs_id(Long ds_id) {
        this.ds_id = ds_id;
    }

    public Long getMhs_id() {
        return mhs_id;
    }

    public void setMhs_id(Long mhs_id) {
        this.mhs_id = mhs_id;
    }

    public String getMk_name() {
        return mk_name;
    }

    public void setMk_name(String mk_name) {
        this.mk_name = mk_name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
