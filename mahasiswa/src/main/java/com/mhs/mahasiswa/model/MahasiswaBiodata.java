package com.mhs.mahasiswa.model;

import jakarta.persistence.*;

@Entity
@Table(name = "mahasiswa_biodatas")
public class MahasiswaBiodata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mhs_id")
    private Long mhs_id;

    @Column(name = "nim", length = 7)
    private int nim;

    @Column(name = "mhs_name", length = 100)
    private String mhs_name;

    @Column(name = "mhs_gender", length = 10)
    private String mhs_gender;

    @Column(name = "mhs_address", columnDefinition = "TEXT")
    private String mhs_address;

    public Long getMhs_id() {
        return mhs_id;
    }

    public void setMhs_id(Long mhs_id) {
        this.mhs_id = mhs_id;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public String getMhs_name() {
        return mhs_name;
    }

    public void setMhs_name(String mhs_name) {
        this.mhs_name = mhs_name;
    }

    public String getMhs_gender() {
        return mhs_gender;
    }

    public void setMhs_gender(String mhs_gender) {
        this.mhs_gender = mhs_gender;
    }

    public String getMhs_address() {
        return mhs_address;
    }

    public void setMhs_address(String mhs_address) {
        this.mhs_address = mhs_address;
    }
}
