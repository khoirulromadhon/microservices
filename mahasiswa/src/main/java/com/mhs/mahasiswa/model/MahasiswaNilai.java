package com.mhs.mahasiswa.model;

import jakarta.persistence.*;

@Entity
@Table(name = "mahasiswa_nilais")
public class MahasiswaNilai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nilai_id")
    private Long nilai_id;

    @Column(name = "mhs_id")
    private Long mhs_id;

    @ManyToOne
    @JoinColumn(name = "mhs_id", insertable = false, updatable = false)
    public MahasiswaBiodata mahasiswaBiodata;

    @Column(name = "mk_id")
    private Long mk_id;

    @ManyToOne
    @JoinColumn(name = "mk_id", insertable = false, updatable = false)
    public Matakuliah matakuliah;

    @Column(name = "nilai_angka")
    private int nilai_angka;

    @Column(name = "nilai_huruf")
    private String nilai_huruf;

    public Long getNilai_id() {
        return nilai_id;
    }

    public void setNilai_id(Long nilai_id) {
        this.nilai_id = nilai_id;
    }

    public Long getMhs_id() {
        return mhs_id;
    }

    public void setMhs_id(Long mhs_id) {
        this.mhs_id = mhs_id;
    }

    public MahasiswaBiodata getMahasiswaBiodata() {
        return mahasiswaBiodata;
    }

    public void setMahasiswaBiodata(MahasiswaBiodata mahasiswaBiodata) {
        this.mahasiswaBiodata = mahasiswaBiodata;
    }

    public Long getMk_id() {
        return mk_id;
    }

    public void setMk_id(Long mk_id) {
        this.mk_id = mk_id;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }

    public int getNilai_angka() {
        return nilai_angka;
    }

    public void setNilai_angka(int nilai_angka) {
        this.nilai_angka = nilai_angka;
    }

    public String getNilai_huruf() {
        return nilai_huruf;
    }

    public void setNilai_huruf(String nilai_huruf) {
        this.nilai_huruf = nilai_huruf;
    }
}
