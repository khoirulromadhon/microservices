package com.mhs.mahasiswa.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mhs.mahasiswa.model.MahasiswaBiodata;
import com.mhs.mahasiswa.service.MhsBioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class MhsBioRestController {
    @Autowired
    private MhsBioService mhsBioService;

    @GetMapping("mhs_bio")
    public ResponseEntity<List<MahasiswaBiodata>> getAllMhsBio(){
        try {
            List<MahasiswaBiodata> mahasiswaBiodataList = this.mhsBioService.getAllMhsBio();
            return new ResponseEntity<>(mahasiswaBiodataList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("mhs_bio/{id}")
    public ResponseEntity<?> getMhsBioById(@PathVariable("id") Long id){
        try {
            MahasiswaBiodata mahasiswaBiodata = this.mhsBioService.getMhsBioById(id);
            return new ResponseEntity<>(mahasiswaBiodata, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("mhs_bio")
    public ResponseEntity<MahasiswaBiodata> saveMhsBio(@RequestBody MahasiswaBiodata mahasiswaBiodata){
        try {
            this.mhsBioService.saveMhsBio(mahasiswaBiodata);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonString = objectMapper.writeValueAsString(mahasiswaBiodata);
            this.mhsBioService.sendMhsBio(jsonString);

            return new ResponseEntity<MahasiswaBiodata>(mahasiswaBiodata, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("mhs_bio/{id}")
    public ResponseEntity<?> updateMhsBio(@RequestBody MahasiswaBiodata mahasiswaBiodata, @PathVariable("id") Long id){
        try {
            mahasiswaBiodata.setMhs_id(id);
            this.mhsBioService.saveMhsBio(mahasiswaBiodata);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonString = objectMapper.writeValueAsString(mahasiswaBiodata);
            this.mhsBioService.sendMhsBio(jsonString);

            Map<String, Object> result = new HashMap<>();
            String message = "Success Update Data !!!!!";
            result.put("Message", message);
            result.put("Data", mahasiswaBiodata);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("mhs_bio/{id}")
    public ResponseEntity<?> deleteMhsBio(@PathVariable("id") Long id){
        try {
            MahasiswaBiodata mahasiswaBiodata = this.mhsBioService.getMhsBioById(id);

            if (mahasiswaBiodata != null) {
                this.mhsBioService.deleteMhsBio(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Success Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
