package com.mhs.mahasiswa.repository;

import com.mhs.mahasiswa.model.MahasiswaBiodata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MhsBioRepositories extends JpaRepository<MahasiswaBiodata, Long> {
}
