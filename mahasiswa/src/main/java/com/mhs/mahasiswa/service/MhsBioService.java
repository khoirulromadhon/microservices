package com.mhs.mahasiswa.service;

import com.mhs.mahasiswa.model.MahasiswaBiodata;
import com.mhs.mahasiswa.repository.MhsBioRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MhsBioService {
    private static final String TOPIC = "mhs_bio";
    @Autowired
    private MhsBioRepositories mhsBioRepositories;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public List<MahasiswaBiodata> getAllMhsBio(){
        return this.mhsBioRepositories.findAll();
    }
    public MahasiswaBiodata getMhsBioById(Long mhs_id){
        return this.mhsBioRepositories.findById(mhs_id).orElse(null);
    }
    public void saveMhsBio(MahasiswaBiodata mahasiswaBiodata){
        this.mhsBioRepositories.save(mahasiswaBiodata);
    }
    public void deleteMhsBio(Long mhs_id){
        this.mhsBioRepositories.deleteById(mhs_id);
    }
    public void sendMhsBio(String data){
        this.kafkaTemplate.send(TOPIC, data);
    }
}
