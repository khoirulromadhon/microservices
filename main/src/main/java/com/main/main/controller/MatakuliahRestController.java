package com.main.main.controller;

import com.main.main.model.Matakuliah;
import com.main.main.service.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class MatakuliahRestController {
    @Autowired
    MatakuliahService matakuliahService;

    @GetMapping("/matakuliah")
    public ResponseEntity<List<Matakuliah>> getAllMatakuliah() {
        try {
            List<Matakuliah> matakuliahList = this.matakuliahService.getAllMatakuliah();
            return new ResponseEntity<>(matakuliahList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/matakuliah/{id}")
    public ResponseEntity<?> getMatakuliahById(@PathVariable("id") Long id) {
        try {
            Matakuliah matakuliah = this.matakuliahService.getMatakuliahById(id);
            return new ResponseEntity<>(matakuliah, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/matakuliah")
    public ResponseEntity<Matakuliah> saveMatakuliah(@RequestBody Matakuliah matakuliah){
        try {
            this.matakuliahService.saveMatakuliah(matakuliah);
            return new ResponseEntity<Matakuliah>(matakuliah, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/matakuliah/{id}")
    public ResponseEntity<?> updateMatakuliah(@RequestBody Matakuliah matakuliah, @PathVariable("id") Long id){
        try {
            matakuliah.setMk_id(id);
            this.matakuliahService.saveMatakuliah(matakuliah);
            Map<String, Object> result = new HashMap<>();
            String message = "Success Update Data !!!!!";
            result.put("Message", message);
            result.put("Data", matakuliah);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/matakuliah/{id}")
    public ResponseEntity<?> deleteMatakuliah(@PathVariable("id") Long id) {
        Matakuliah matakuliah = this.matakuliahService.getMatakuliahById(id);

        try {
            if (matakuliah != null){
                this.matakuliahService.deleteMatakuliah(id);
                return ResponseEntity.status(HttpStatus.OK).body("Success Delete Data !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
