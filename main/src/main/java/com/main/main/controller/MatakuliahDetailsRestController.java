package com.main.main.controller;

import com.main.main.model.MatakuliahDetails;
import com.main.main.service.MatakuliahDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class MatakuliahDetailsRestController {
    @Autowired
    private MatakuliahDetailsService matakuliahDetailsService;

    @GetMapping("/matakuliah_details")
    public ResponseEntity<List<MatakuliahDetails>> getAllMatakuliahDetails() {
        try {
            List<MatakuliahDetails> matakuliahDetailList = this.matakuliahDetailsService.getAllMatakuliahDetails();
            return new ResponseEntity<>(matakuliahDetailList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/matakuliah_details/{id}")
    public ResponseEntity<?> getMatakuliahDetailById(@PathVariable("id") Long id) {
        try {
            MatakuliahDetails matakuliahDetails = this.matakuliahDetailsService.getMatakuliahDetailsById(id);
            return new ResponseEntity<>(matakuliahDetails, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/matakuliah_details")
    public ResponseEntity<MatakuliahDetails> saveMatakuliahDetails(@RequestBody MatakuliahDetails matakuliahDetails) {
        try {
            this.matakuliahDetailsService.saveMatakuliahDetails(matakuliahDetails);
            return new ResponseEntity<MatakuliahDetails>(matakuliahDetails, HttpStatus.OK);
        }
        catch (Exception exception) {
            HashMap<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/matakuliah_details")
    public ResponseEntity<?> updateMatakuliahDetails(@PathVariable("id") Long id, @RequestBody MatakuliahDetails matakuliahDetails) {
        try {
            matakuliahDetails.setMd_id(id);
            this.matakuliahDetailsService.saveMatakuliahDetails(matakuliahDetails);
            Map<String, Object> result = new HashMap<>();
            String message = "Success Update Data !!!!!";
            result.put("Message", message);
            result.put("Data", matakuliahDetails);
            return new ResponseEntity(result, HttpStatus.OK);
        }
        catch (Exception exception) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/matakuliah_details")
    public ResponseEntity<?> deteleMatakuliahDetails(@PathVariable("id") Long id) {
        MatakuliahDetails matakuliahDetails = this.matakuliahDetailsService.getMatakuliahDetailsById(id);

        try {
            if (matakuliahDetails != null) {
                this.matakuliahDetailsService.deleteMatakuliahDetails(id);
                return ResponseEntity.status(HttpStatus.OK).body("Success Delete Data !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found !!!!!");
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
