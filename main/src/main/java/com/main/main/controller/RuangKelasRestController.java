package com.main.main.controller;

import com.main.main.model.RuangKelas;
import com.main.main.service.RuangKelasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class RuangKelasRestController {
    @Autowired
    RuangKelasService ruangKelasService;

    @GetMapping("ruang_kelas")
    public ResponseEntity<List<RuangKelas>> getAllRuangKelas() {
        try {
            List<RuangKelas> ruangKelasList = this.ruangKelasService.getAllRuangKelas();
            return new ResponseEntity<>(ruangKelasList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("ruang_kelas/{id}")
    public ResponseEntity<?> getRuangKelasById(@PathVariable("id") Long id) {
        try {
            RuangKelas ruangKelas = this.ruangKelasService.getRuangKelasById(id);
            return new ResponseEntity<>(ruangKelas, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("ruang_kelas")
    public ResponseEntity<RuangKelas> saveRuangKelas(@RequestBody RuangKelas ruangKelas) {
        try {
            this.ruangKelasService.saveRuangKelas(ruangKelas);
            return new ResponseEntity<RuangKelas>(ruangKelas, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("ruang_kelas/{id}")
    public ResponseEntity<?> updateRuangKelas(@RequestBody RuangKelas ruangKelas, @PathVariable("id") Long id) {
        try {
            ruangKelas.setRk_id(id);
            this.ruangKelasService.saveRuangKelas(ruangKelas);
            Map<String, Object> result = new HashMap<>();
            String message = "Success Update Data !!!!!";
            result.put("Message", message);
            result.put("Data", ruangKelas);
            return new ResponseEntity<>(ruangKelas, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("ruang_kelas/{id}")
    public ResponseEntity<?> deleteRuangKelas(@PathVariable("id") Long id) {
        try {
            RuangKelas ruangKelas = this.ruangKelasService.getRuangKelasById(id);

            if (ruangKelas != null){
                this.ruangKelasService.deleteRuangKelas(id);
                return ResponseEntity.status(HttpStatus.OK).body("Data Success Deleted !!!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
