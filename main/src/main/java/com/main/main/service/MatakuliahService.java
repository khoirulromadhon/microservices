package com.main.main.service;

import com.main.main.model.Matakuliah;
import com.main.main.repository.MatakuliahRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatakuliahService {
    @Autowired
    MatakuliahRepositories matakuliahRepositories;

    public List<Matakuliah> getAllMatakuliah(){
        return this.matakuliahRepositories.findAll();
    }
    public Matakuliah getMatakuliahById(Long mk_id){
        return this.matakuliahRepositories.findById(mk_id).orElse(null);
    }
    public void saveMatakuliah(Matakuliah matakuliah) {
        this.matakuliahRepositories.save(matakuliah);
    }
    public void deleteMatakuliah(Long mk_id) {
        this.matakuliahRepositories.deleteById(mk_id);
    }
}
