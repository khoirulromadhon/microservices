package com.main.main.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.main.main.model.MahasiswaBiodata;
import com.main.main.repository.MahasiswaBiodataRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaBiodataService {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private MahasiswaBiodataRepositories mahasiswaBiodataRepositories;

    @KafkaListener(topics = "mhs_bio", groupId = "mhs_bio_group")
    public void consumerMhsBio(String data) throws JsonProcessingException {
        MahasiswaBiodata mahasiswaBiodata = objectMapper.readValue(data, MahasiswaBiodata.class);
        this.mahasiswaBiodataRepositories.save(mahasiswaBiodata);
    }
}
