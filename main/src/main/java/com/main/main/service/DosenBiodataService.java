package com.main.main.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.main.main.model.DosenBiodata;
import com.main.main.repository.DosenBiodataRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class DosenBiodataService {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private DosenBiodataRepositories dosenBiodataRepositories;

    @KafkaListener(topics = "dosen_bio", groupId = "dosen_bio_group")
    public void consumerDosenBio(String data) throws JsonProcessingException {
        DosenBiodata dosenBiodata = objectMapper.readValue(data, DosenBiodata.class);
        this.dosenBiodataRepositories.save(dosenBiodata);
    }
}
