package com.main.main.service;

import com.main.main.model.MatakuliahDetails;
import com.main.main.repository.MatakuliahDetailRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class MatakuliahDetailsService {
    @Autowired
    private MatakuliahDetailRepositories matakuliahDetailRepositories;

    public List<MatakuliahDetails> getAllMatakuliahDetails() {
        return this.matakuliahDetailRepositories.findAll();
    }
    public MatakuliahDetails getMatakuliahDetailsById(Long md_id) {
        return this.matakuliahDetailRepositories.findById(md_id).orElse(null);
    }
    public void saveMatakuliahDetails(MatakuliahDetails matakuliahDetails) {
        this.matakuliahDetailRepositories.save(matakuliahDetails);
    }
    public void deleteMatakuliahDetails(Long md_id) {
        this.matakuliahDetailRepositories.deleteById(md_id);
    }
}
