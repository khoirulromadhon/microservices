package com.main.main.service;

import com.main.main.model.RuangKelas;
import com.main.main.repository.RuangKelasRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RuangKelasService {
    @Autowired
    private RuangKelasRepositories ruangKelasRepositories;

    public List<RuangKelas> getAllRuangKelas() {
        return this.ruangKelasRepositories.findAll();
    }
    public RuangKelas getRuangKelasById(Long rk_id){
        return this.ruangKelasRepositories.findById(rk_id).orElse(null);
    }
    public void saveRuangKelas(RuangKelas ruangKelas){
        this.ruangKelasRepositories.save(ruangKelas);
    }
    public void deleteRuangKelas(Long rk_id){
        this.ruangKelasRepositories.deleteById(rk_id);
    }
}
