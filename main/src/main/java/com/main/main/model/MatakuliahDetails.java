package com.main.main.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "matakuliah_details")
public class MatakuliahDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "md_id")
    private Long md_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mk_id")
    private Matakuliah matakuliah;

    @Column(name = "mk_id", insertable = false, updatable = false)
    private Long mk_id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rk_id")
    private List<RuangKelas> ruangKelasList;

    @Column(name = "rk_id", insertable = false, updatable = false)
    private Long rk_id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ds_id")
    private List<DosenBiodata> dosenBiodataList;

    @Column(name = "ds_id", insertable = false, updatable = false)
    private Long ds_id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "mhs_id")
    private List<MahasiswaBiodata> mahasiswaBiodataList;

    @Column(name = "mhs_id", insertable = false, updatable = false)
    private Long mhs_id;

    public Long getMd_id() {
        return md_id;
    }

    public void setMd_id(Long md_id) {
        this.md_id = md_id;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }

    public Long getMk_id() {
        return mk_id;
    }

    public void setMk_id(Long mk_id) {
        this.mk_id = mk_id;
    }

    public List<RuangKelas> getRuangKelasList() {
        return ruangKelasList;
    }

    public void setRuangKelasList(List<RuangKelas> ruangKelasList) {
        this.ruangKelasList = ruangKelasList;
    }

    public Long getRk_id() {
        return rk_id;
    }

    public void setRk_id(Long rk_id) {
        this.rk_id = rk_id;
    }

    public List<DosenBiodata> getDosenBiodataList() {
        return dosenBiodataList;
    }

    public void setDosenBiodataList(List<DosenBiodata> dosenBiodataList) {
        this.dosenBiodataList = dosenBiodataList;
    }

    public Long getDs_id() {
        return ds_id;
    }

    public void setDs_id(Long ds_id) {
        this.ds_id = ds_id;
    }

    public List<MahasiswaBiodata> getMahasiswaBiodataList() {
        return mahasiswaBiodataList;
    }

    public void setMahasiswaBiodataList(List<MahasiswaBiodata> mahasiswaBiodataList) {
        this.mahasiswaBiodataList = mahasiswaBiodataList;
    }

    public Long getMhs_id() {
        return mhs_id;
    }

    public void setMhs_id(Long mhs_id) {
        this.mhs_id = mhs_id;
    }
}
