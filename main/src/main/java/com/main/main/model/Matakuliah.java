package com.main.main.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "matakuliah")
public class Matakuliah {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mk_id")
    private Long mk_id;

    @Column(name = "mk_name", length = 100)
    private String mk_name;

    @Column(name = "duration")
    private int duration;

    public Long getMk_id() {
        return mk_id;
    }

    public void setMk_id(Long mk_id) {
        this.mk_id = mk_id;
    }

    public String getMk_name() {
        return mk_name;
    }

    public void setMk_name(String mk_name) {
        this.mk_name = mk_name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
