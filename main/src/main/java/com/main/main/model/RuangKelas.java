package com.main.main.model;

import jakarta.persistence.*;

@Entity
@Table(name = "ruang_kelas")
public class RuangKelas {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rk_id")
    private Long rk_id;

    @Column(name = "rk_code", length = 6)
    private String rk_code;

    @Column(name = "rk_name", length = 50)
    private String rk_name;

    @Column(name = "rk_description", columnDefinition = "TEXT")
    private String rk_description;

    public Long getRk_id() {
        return rk_id;
    }

    public void setRk_id(Long rk_id) {
        this.rk_id = rk_id;
    }

    public String getRk_code() {
        return rk_code;
    }

    public void setRk_code(String rk_code) {
        this.rk_code = rk_code;
    }

    public String getRk_name() {
        return rk_name;
    }

    public void setRk_name(String rk_name) {
        this.rk_name = rk_name;
    }

    public String getRk_description() {
        return rk_description;
    }

    public void setRk_description(String rk_description) {
        this.rk_description = rk_description;
    }
}
