package com.main.main.repository;

import com.main.main.model.MahasiswaBiodata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MahasiswaBiodataRepositories extends JpaRepository<MahasiswaBiodata, Long> {
}
