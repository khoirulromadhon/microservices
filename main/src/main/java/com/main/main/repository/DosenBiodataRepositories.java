package com.main.main.repository;

import com.main.main.model.DosenBiodata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenBiodataRepositories extends JpaRepository<DosenBiodata, Long> {
}
