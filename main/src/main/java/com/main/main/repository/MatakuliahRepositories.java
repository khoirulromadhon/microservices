package com.main.main.repository;

import com.main.main.model.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatakuliahRepositories extends JpaRepository<Matakuliah, Long> {
}
