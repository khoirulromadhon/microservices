package com.main.main.repository;

import com.main.main.model.MatakuliahDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatakuliahDetailRepositories extends JpaRepository<MatakuliahDetails, Long> {
}
