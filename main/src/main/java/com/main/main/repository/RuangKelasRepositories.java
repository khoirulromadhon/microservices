package com.main.main.repository;

import com.main.main.model.RuangKelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuangKelasRepositories extends JpaRepository<RuangKelas, Long> {
}
