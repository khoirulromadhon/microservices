package com.dsn.dosen.model;

import jakarta.persistence.*;

@Entity
@Table(name = "dosen_biodatas")
public class DosenBiodata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dosen_id")
    private Long dosen_id;

    @Column(name = "nip", length = 12)
    private long nip;

    @Column(name = "dosen_name", length = 100)
    private String dosen_name;

    @Column(name = "dosen_gender", length = 10)
    private String dosen_gender;

    @Column(name = "dosen_address", columnDefinition = "TEXT")
    private String dosen_address;

    public Long getDosen_id() {
        return dosen_id;
    }

    public void setDosen_id(Long dosen_id) {
        this.dosen_id = dosen_id;
    }

    public long getNip() {
        return nip;
    }

    public void setNip(long nip) {
        this.nip = nip;
    }

    public String getDosen_name() {
        return dosen_name;
    }

    public void setDosen_name(String dosen_name) {
        this.dosen_name = dosen_name;
    }

    public String getDosen_gender() {
        return dosen_gender;
    }

    public void setDosen_gender(String dosen_gender) {
        this.dosen_gender = dosen_gender;
    }

    public String getDosen_address() {
        return dosen_address;
    }

    public void setDosen_address(String dosen_address) {
        this.dosen_address = dosen_address;
    }
}
