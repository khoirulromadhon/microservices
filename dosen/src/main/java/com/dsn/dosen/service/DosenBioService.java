package com.dsn.dosen.service;

import com.dsn.dosen.model.DosenBiodata;
import com.dsn.dosen.repository.DosenBiodataRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DosenBioService {
    private static final String TOPIC = "dosen_bio";
    @Autowired
    private DosenBiodataRepositories dosenBiodataRepositories;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public List<DosenBiodata> getAllDosenBio(){
        return this.dosenBiodataRepositories.findAll();
    }
    public DosenBiodata getDosenBioById(Long dosen_id){
        return this.dosenBiodataRepositories.findById(dosen_id).orElse(null);
    }
    public void saveDosenBio(DosenBiodata dosenBiodata) {
        this.dosenBiodataRepositories.save(dosenBiodata);
    }
    public void deleteDosenBio(Long dosen_id) {
        this.dosenBiodataRepositories.deleteById(dosen_id);
    }
    public void sendDosenBio(String data) {
        this.kafkaTemplate.send(TOPIC, data);
    }
}
