package com.dsn.dosen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DosenApplication {

	public static void main(String[] args) {
		SpringApplication.run(DosenApplication.class, args);
	}

}
