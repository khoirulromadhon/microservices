package com.dsn.dosen.controller;

import com.dsn.dosen.model.DosenBiodata;
import com.dsn.dosen.service.DosenBioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class DosenBioRestController {
    @Autowired
    private DosenBioService dosenBioService;

    @GetMapping("dosen_bio")
    public ResponseEntity<List<DosenBiodata>> getAllDosenBio(){
        try {
            List<DosenBiodata> dosenBiodataList = this.dosenBioService.getAllDosenBio();
            return new ResponseEntity<>(dosenBiodataList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("dosen_bio/{id}")
    public ResponseEntity<?> getDosenBioById(@PathVariable("id") Long id) {
        try {
            DosenBiodata dosenBiodata = this.dosenBioService.getDosenBioById(id);
            return new ResponseEntity<>(dosenBiodata, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("dosen_bio")
    public ResponseEntity<DosenBiodata> saveDosenBio(@RequestBody DosenBiodata dosenBiodata){
        try {
            this.dosenBioService.saveDosenBio(dosenBiodata);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonString = objectMapper.writeValueAsString(dosenBiodata);
            this.dosenBioService.sendDosenBio(jsonString);

            return new ResponseEntity<DosenBiodata>(dosenBiodata, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("dosen_bio/{id}")
    public ResponseEntity<?> updateDosenBio(@RequestBody DosenBiodata dosenBiodata, @PathVariable("id") Long id){
        try {
            dosenBiodata.setDosen_id(id);
            this.dosenBioService.saveDosenBio(dosenBiodata);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonString = objectMapper.writeValueAsString(dosenBiodata);
            this.dosenBioService.sendDosenBio(jsonString);

            Map<String, Object> result = new HashMap<>();
            String message = "Success Update Data !!!!!";
            result.put("Message", message);
            result.put("Data", dosenBiodata);
            return new ResponseEntity(result, HttpStatus.OK);
        }
        catch (Exception exception) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("dosen_bio/{id}")
    public ResponseEntity<?> deleteDosenBio(@PathVariable("id") Long id){
        try {
            DosenBiodata dosenBiodata = this.dosenBioService.getDosenBioById(id);

            if (dosenBiodata != null){
                this.dosenBioService.deleteDosenBio(id);
                return ResponseEntity.status(HttpStatus.OK).body("Deleted Data Success !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Data Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
