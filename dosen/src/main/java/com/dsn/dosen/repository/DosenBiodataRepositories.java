package com.dsn.dosen.repository;

import com.dsn.dosen.model.DosenBiodata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenBiodataRepositories extends JpaRepository<DosenBiodata, Long> {
}
